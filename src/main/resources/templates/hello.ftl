<#assign base = request.contextPath />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="fyunli">

    <base id="base" href="${base}">
    <title>Spring Boot - hello</title>

    <!-- Bootstrap core CSS -->
    <link href="${base}/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="${base}/css/main.css" rel="stylesheet">

</head>
<body>
        <p>
            welcome ${name} to freemarker!
        </p>

        <div class="container">
            <div class="page-header">
                <h1>Sprint Boot: Hello</h1>
            </div>
        </div>

        <p>性别：
        <#if gender==0>
            女
        <#elseif gender==1>
            男
        <#else>
            保密
        </#if>
        </p>


        <h4>我的好友：</h4>
        <#list friends as item>
        姓名：${item.name} , 年龄${item.age}
        <br>
        </#list>
        <footer class="footer">
            <div class="container">
                <p class="text-muted">©201 zhuAnrong</p>
            </div>
        </footer>
		<script src="${base}/bootstrap/js/jquery-3.2.1.js"></script>
        <script src="${base}/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>